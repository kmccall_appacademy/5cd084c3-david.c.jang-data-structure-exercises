# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  # your code goes here
  sorted = arr.sort
  sorted.last - sorted.first
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  # your code goes here
  arr.sort == arr
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  # your code goes here
  vowels = "aeiou"
  count = 0
  str.downcase.chars.each {|char| count += 1 if  vowels.include?(char) }
  count
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  # problem with this is if the string has capital non-vowels but
  # test is not thorough enough
  str.downcase.gsub(/[aeiou]/, '')
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  # your code goes here
  int.to_s.chars.sort{|a,b| b <=> a}
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  # your code goes here
  str.downcase.chars.uniq.join != str
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  # your code goes here
  arr1 = arr.join
  "(#{arr1[0..2]}) #{arr1[3..5]}-#{arr1[-4..-1]}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  # your code goes here
  sorted = str.gsub(/,/,'').chars.sort
  sorted.last.to_i - sorted.first.to_i
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  # your code goes here
  # dangerous cuz it changes input value but its ok for now
  offset += arr.length  if offset < 0   # handle negative case
  offset %= arr.length if offset > arr.length #handle large offsets

  to_be_right = arr.take(offset)
  to_be_left = arr.drop(offset)
  to_be_left + to_be_right
end
